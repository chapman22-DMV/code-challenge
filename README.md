## Front End Code Challenge
This project is my attempt at the SingleStone Front End Web Developer Code Challenge.
I approached this challenge with the idea of being as minimalistic as possible - No CSS Framework, No JS Framework/Library.
It's been awhile since I've relied solely on html and css to bring a comp to life, so I decided this would be a good exercise to try.

## Demo
https://code-challenge.azurewebsites.net

## Installation
 - git clone
 - npm install
 - npm start
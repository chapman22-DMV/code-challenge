var gulp = require('gulp'),
	watch = require('gulp-watch'),
	concat= require('gulp-concat'),
	sass = require('gulp-sass'),
	minify = require('gulp-minify-css'),
	notify =  require('gulp-notify');
	uglify = require('gulp-uglify');

gulp.task('sass', function () {
  return gulp.src('styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(minify({benchmark:true,processImport:false}))
    .pipe(gulp.dest('./dist/css'))
    .pipe(notify("Sass Complete"));
});


gulp.task('default',['sass'], function(){
	gulp.watch(['styles/**/*.scss'], ['sass']);
})